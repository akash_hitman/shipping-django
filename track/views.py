# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from helpers import saveShipDetails, getTrackDetails, getAllCouriers
from django.http import JsonResponse
from json import dumps

def track(request, slug, trackId):
	try:
		if slug and trackId:
			if request.method == 'POST':
				data = saveShipDetails(slug, trackId)
				return JsonResponse({
					"data": str(data),
					"hint": "success"
				}, content_type='application/json', status=201, safe=False)
			elif request.method == 'GET':
				data = getTrackDetails(slug, trackId)
				return JsonResponse({
					"data": data,
					"hint": "success"
				}, content_type='application/json', status=200, safe=False)
		else:
			raise Exception("Check slug or trackId !!!")
	except Exception as e:
		# print dir(e)
		return JsonResponse({
			"data": [],
			"hint": "Success"
		}, status=200)

def getCourier(request):
	try:
		if request.method == 'GET':
			data = getAllCouriers()
			return JsonResponse({
				"data": data,
				"hint": "All Couriers"
			}, status=200, content_type="application/json", safe=False)
		else: 
			raise Exception('This request has no post method') 
	except Exception as e:
		return JsonResponse({
			"data": [],
			"hint": e.message
		}, status=500, content_type="application/json", safe=False)
