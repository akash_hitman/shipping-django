# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.forms.models import model_to_dict

# Create your models here.
class AfterShip(models.Model):

	id =  models.CharField(max_length=255, primary_key=True)
	track_id = models.CharField(max_length=255, null=True)
	slug = models.CharField(max_length=255, null=True)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)
	deleted_at = models.DateTimeField(null=True)

	class Meta :
		db_table = 'after_ship'

	def __str__(self):
		return "id={id} , slug={slug}, trackId={trackId}".format(id=str(self.id),
			slug=self.slug,
			trackId=self.track_id
		)