from uuid import uuid4
from models import AfterShip
from aftership.afterShip import APIv4
from env import AFTERSHIP_API_KEY

def saveShipDetails(slug, trackId):
	return AfterShip.objects.create(**{
		'id': uuid4(),
		'track_id': trackId,
		'slug': slug
	})

def getTrackDetails(slug, trackId):
	api = APIv4(AFTERSHIP_API_KEY)
	saveShipDetails(slug, trackId)
	data = api.trackings.get(slug, trackId, fields=['title', 'created_at'])
	return data

def getAllCouriers():
	api = APIv4(AFTERSHIP_API_KEY)
	return api.couriers.all.get()