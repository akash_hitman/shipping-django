from django.conf.urls import url
from views import *

urlpatterns = [
    # url(r'^', Track),
    url(r'(?P<slug>[0-9a-z-]+)/(?P<trackId>[a-z-A-Z0-9-]+)/', track),
    url(r'couriers/', getCourier)
]
