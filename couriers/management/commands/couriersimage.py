from django.core.management.base import BaseCommand, CommandError
from couriers.couriers_image import save_couriers_image

class Command(BaseCommand):
	help = 'Download All Couries SVG image'

	def add_arguments(self, parser):
	    parser.add_argument('--filetype', type=str)

	def handle(self, *args, **options):
		# print options['filetype']
		save_couriers_image(options['filetype'])
