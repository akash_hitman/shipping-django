from django.core.management.base import BaseCommand, CommandError
from couriers.couriers_fixture import generate_couriers_fixture

class Command(BaseCommand):
	help = 'Generate Couriers\'s Fexture'

	def add_arguments(self, parser):
	    parser.add_argument('--name', type=str)
	    parser.add_argument('--class', type=str)


	def handle(self, *args, **options):
		generate_couriers_fixture(options)
