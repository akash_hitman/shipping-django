# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.

class Couriers(models.Model):
	id = models.CharField(max_length=255, primary_key=True)
	rank = models.IntegerField(blank=True, null=True)
	name = models.CharField(max_length=255, unique=True, null=False)
	slug = models.CharField(max_length=255, unique=True, null=False)
	phone = models.CharField(max_length=255, null=True)
	special = models.BooleanField(default=False)
	site_url = models.CharField(max_length=100, null=False)
	image_url = models.CharField(max_length=255, unique=True)
	created_at = models.DateTimeField(auto_now_add=True, null=True)
	updated_at = models.DateTimeField(auto_now=True, null=True)
	deleted_at = models.DateTimeField(null=True)
	
	class Meta:
		db_table = 'couriers'
