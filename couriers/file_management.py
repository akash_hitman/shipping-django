from couriers.apps import CouriersConfig

def createFolder(directory):
	from os import makedirs
	try:
		makedirs(directory)
	except OSError as e:
		pass

def findFile(path, fileName):
	realPath = '{path}/{fileName}'.format(path=path, fileName=fileName)
	try:
		fixture = open(realPath,'r')
		fixture.close()
		raise Exception('sp_file_exist')
	except Exception as e:
		if e.message == 'sp_file_exist':
			raise e
		else:
			raise Exception('not_found')

def findMultipleFile(path, fileName):
	realPath = '{path}/{fileName}'.format(path=path, fileName=fileName)
	from glob import glob
	try:
		count = len(glob(realPath))
		if count:
			print "there is {count} {fileName} file".format(count=count, fileName=fileName)
			raise Exception('file_exist')
		else:
			raise Exception('not_found')
	except Exception as e:
		raise e

def fixture_dir_path():
	return "{root}/{dir}".format(root=CouriersConfig.name, dir='fixtures')

def generate_hash(hashLength):
	from random import choice
	alphanumaric = 'abcdefghijklmnopqrstuvwxyz0123456789'
	return ''.join(choice(alphanumaric) for _ in xrange(hashLength))

def write_into_file(directory, fileName, fixtureDataList):
	file_path = '{path}/{name}'.format(path=directory, name=fileName)
	with open(file_path, 'w') as file:
		import json
		file.write(json.dumps(fixtureDataList, file, indent=4))

def create_file_with_extension(directory, fileName, extension, data):
	file = '{path}/{name}.{ext}'.format(path=directory, name=fileName, ext=extension)
	try:
		with open(file, 'w') as f:
			f.write(data[1:])
	except Exception as e:
		import os
		os.remove(file)
		raise e