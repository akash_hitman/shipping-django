def to_server(object):
	return {
		"rank": 0,
		"name": object['name'],
		"slug": object['slug'],
		"phone": object['phone'],
		"special": False,
		"site_url": object['web_url'],
		"image_url": "http://assets.aftership.com/couriers/svg/{slug}.svg".format(slug=object['slug']),
		"created_at": None,
		"updated_at": None,
		"deleted_at": None
	}

def to_client(object):
	return {
		"id": object['id'],
		"rank": object['rank'],
		"name": object['name'],
		"slug": object['slug'],
		"phone": object['phone'],
		"special": object['special'],
		"siteUrl": object['site_url'],
		"imageUrl": object['image_url'],
		"createdAt": object['created_at'],
		"updatedAt": object['updated_at'],
		"deletedAt": object['deleted_at']
	}