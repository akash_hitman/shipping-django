from models import Couriers
from aftership.Repository import fetchAll
from track.helpers import getAllCouriers
from env import DIRECTORY, AFTERSHIP_IMAGE_BASE_URL
import requests
from file_management import *


# def writeIntoJsonFile(path, fileName):
# 	# print getAllCouriers()['couriers']
# 	realPath = '{path}/{fileName}'.format(path=path, fileName=fileName)
# 	fixture = open(realPath,'w')
# 	fixture.close()

# def createFile(path, fileName):
# 	realPath = '{path}/{fileName}'.format(path=path, fileName=fileName)
# 	fixture = open(realPath,'w')
# 	fixture.close()

def get_alphanumeric():
	from random import choice
	alphanumaric = 'abcdef0123456789abcdef0123456789'
	return ''.join(choice(alphanumaric) for _ in xrange(43))


def trackingex_headers(image):
	return {
		"accept" : "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
		"cookie": "__cfduid={alphanumaric}".format(alphanumaric=get_alphanumeric()),
		"upgrade-insecure-requests": "1",
		"user-agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36"
	}



headers = {
	"Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
	"Accept-Encoding": "gzip, deflate",
	"Accept-Language": "en-GB,en;q=0.8,en-US;q=0.6,hi;q=0.4",
	"Cache-Control": "max-age=0",
	"Connection": "keep-alive",
	"Cookie": "__cfduid={alphanumaric}".format(alphanumaric=get_alphanumeric()),
	"Host": "assets.aftership.com",
	"Upgrade-Insecure-Requests": "1",
	"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36"
}


# def download_couriers_png_images():
# 	for courier in couriers:
# 		couriers_name = courier['slug']
# 		image = "{}.png".format(couriers_name)
# 		full_path = "{directory}/{image}".format(directory=directory, image=image)
# 		url = "https://www.trackingex.com/Public/Images/couriers/{image}".format(image=image)
# 		file_name = url.split('/')[-1] 
# 		print "Downloading file: %s"%file_name
# 		r = requests.get(url, headers=trackingex_headers(), stream = True)
# 		with open(full_path, 'wb') as f:
# 			for chunk in r.iter_content(chunk_size = 1024*1024):
# 				if chunk:
# 					f.write(chunk)
# 		print "%s : downloaded!\n"%file_name


def download_couriers_images(directory, fileName):
	couriers = getCouriers() if fileName[0] == '*' else [{
		"slug": fileName[0]
	}]
	i = 1
	for courier in couriers:
		couriers_name = courier['slug']
		image = "{name}.{ext}".format(name=couriers_name, ext=fileName[1])
		full_path = "{directory}/{image}".format(directory=directory, image=image)
		RelUrl = "{base_url}{image}".format(base_url=AFTERSHIP_IMAGE_BASE_URL, image=image)
		# RelUrl = "{base_url}{image}".format(base_url=TRACKING_MORE_IMAGE_BASE_URL, image=image)
		result = requests.get(RelUrl, headers=headers)
		print "{index}) {name} processing".format(index=i, name=couriers_name)
		if result.status_code==200:
			try:
				with open(full_path, 'wb') as file:
					file.write(result.text)
			except Exception as e:
				print e
				print "prob due to {}".format(couriers_name)
			print "{index}) {name} complete!!!\n".format(index=i, name=couriers_name)
		else:
			print "status code = {status_code} \n Please check out the base url or image type.".format(status_code=result.status_code)
			break
		i = i + 1
	print "download finish"
	

def save_couriers_image(fileName):
	FILENAME = fileName.split('.')
	createFolder(DIRECTORY)
	try:
		if FILENAME[0] == '*':
			findMultipleFile(DIRECTORY, fileName)
		else:
			findFile(DIRECTORY, fileName)
	except Exception as e:
		if e.message == 'not_found':
			# createFile(DIRECTORY, FILENAME)
			download_couriers_images(DIRECTORY, FILENAME)
		elif e.message == 'file_exist':
			print "Please Delete {directory} and run the command.".format(directory=DIRECTORY)
		elif e.message == 'sp_file_exist':
			print "{fileName} already exist in {directory} directory.".format(fileName=fileName, directory=DIRECTORY)


def getCouriers():
	couriers = getAllCouriers()['couriers']
	return couriers



# def getCouriers():
# 	try:
# 		data = list(fetchAll(Couriers))
# 		if data == []:
# 			# data = save_couriers()
# 			raise Exception('Please Run fixture')
# 		else:
# 			return data
# 	except Exception as e:
# 		raise Exception(e.message)