from couriers_image import getCouriers
from file_management import *
from couriers.models import *
from couriers.transformers import couriers

className = ''
def model_property_fixture(model, object):
	data = {}
	data['model'] = str(model._meta)
	data['pk'] = generate_hash(32)
	data['fields'] = couriers.to_server(object)
	return data


def create_couriers_fixture(dirname, fileName):
	couriers = getCouriers()
	db_couriers = []
	for courier in couriers:
		fixtureData = model_property_fixture(Couriers, courier)
		db_couriers.append(fixtureData)
	write_into_file(dirname, fileName, db_couriers)
	print "Fixture Done"

def generate_couriers_fixture(option):
	fileName = option['name']
	full_dir_path = fixture_dir_path()
	FILENAME = fileName.split('.')
	createFolder(full_dir_path)
	try:
		if FILENAME[-1] != "json":
			raise Exception('ext_err')
		if FILENAME[0] == '*':
			findMultipleFile(full_dir_path, fileName)
		else:
			findFile(full_dir_path, fileName)
	except Exception as e:
		if e.message == 'not_found':
			create_couriers_fixture(full_dir_path, fileName)
		elif e.message == 'file_exist':
			print "Please Delete {directory} and run the command.".format(directory=full_dir_path.split('/')[-1])
		elif e.message == 'ext_err':
			print "Other than json no extension is allowed."
		elif e.message == 'sp_file_exist':
			print "{fileName} already exist in {directory} directory.".format(fileName=fileName, directory=full_dir_path.split('/')[-1])
