from django.core.management.base import BaseCommand, CommandError
from templateGenerator.generator import template_generator

class Command(BaseCommand):
	help = 'Generate the html template of any extension'

	def add_arguments(self, parser):
	    parser.add_argument('--extension', type=str)
	    parser.add_argument('--folder', type=str)

	def handle(self, *args, **options):
		template_generator(options)
