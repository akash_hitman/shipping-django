from couriers.file_management import *
from env import TEMPLATE_NAME
from couriers.models import Couriers

def create_file(directory, extension):
	couriers = list(Couriers.objects.values_list('slug', 'name', 'image_url'))
	# print list(couriers)
	file = 'templateGenerator/{folder}/{name}'.format(folder='templateFolder', name=TEMPLATE_NAME)
	try:
		templateStruct = ''
		with open(file, 'r') as f:
			templateStruct = f.read()
		for index, courier in enumerate(couriers):
			try:
				template = str(templateStruct).format(courierImg=str(courier[2]), name=courier[1].encode('ascii', 'ignore'))
				template = template[:-1] + """<script>
export default {
  layout: 'nav'
}
</script>"""
				fileName = courier[0]+'-tracking'
				create_file_with_extension(directory, fileName, extension, template)
			except Exception as e:
				print e
		print "template generated"
	except Exception as e:
		raise e


def template_generator(options):
	try:
		if options['folder'] and options['extension']:
			base_path = 'templateGenerator/{folder}'.format(folder=options['folder'])
			createFolder(base_path)
			try:
				findMultipleFile(base_path, '*')
			except Exception as e:
				if e.message == 'not_found':
					create_file(base_path, options['extension'])
				else:
					raise e
		else:
			err = ''
			if not options['folder'] and not options['extension']:
				err = '{}_not_found'.format('folder_extension_not_found')
			elif not options['folder']:
				err = '{}_not_found'.format('folder_not_found')
			elif not options['extension']:
				err = '{}_not_found'.format('extension_not_found')
			raise Exception(err)
	except Exception as e:
		if e.message == 'file_exist':
			print "Please Delete {directory} and run the command.".format(directory=options['folder'])
		else:
			print e.message