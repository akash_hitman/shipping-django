from django.http import JsonResponse

def success(data, status, hint, mystatus):
	return JsonResponse({
			"data": data,
			"hint": hint,
			"hintType": "success",
			"statusCode": 'as_{}'.format(mystatus)
		}, 
		content_type='application/json', 
		status=status, 
		safe=False
	)

def failed(status, hint, mystatus):
	return JsonResponse({
			"data": [],
			"hint": hint,
			"hintType": "failed",
			"statusCode": 'as_{}'.format(mystatus)
		}, 
		content_type='application/json',
		status=status,
		safe=False
	)