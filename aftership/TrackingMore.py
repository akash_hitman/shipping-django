# import urllib.request
import requests
from env import TRACKING_MORE_API_KEY

headers={"Content-Type":"application/json",
		"Trackingmore-Api-Key":	TRACKING_MORE_API_KEY,
		'X-Requested-With':'XMLHttpRequest'
		}
class Track:

	def trackingmore(requestData, urlStr, method):
		
		if method == "get":
			url = 'http://api.trackingmore.com/v2/trackings/get'
			RelUrl = url + urlStr
			result = requests.get(RelUrl, headers=headers)
			# req = urllib.request.Request(RelUrl, headers=headers)
			# result = urllib.request.urlopen(req).read()
		
		elif method == "batch":
			url = 'http://api.trackingmore.com/v2/trackings/batch'
			RelUrl = url + urlStr
			result = requests.post(RelUrl, data=requestData.encode('utf-8'), headers=headers)
			# req = urllib.request.Request(RelUrl,requestData.encode('utf-8'), headers=headers,method="POST")
			# result = urllib.request.urlopen(req).read()
		
		elif method == "codeNumberGet":
			url = 'http://api.trackingmore.com/v2/trackings'
			RelUrl = url + urlStr
			result = requests.get(RelUrl, data=requestData.encode('utf-8'), headers=headers)
			# req = urllib.request.Request(RelUrl,requestData.encode('utf-8'), headers=headers,method="GET")
			# result = urllib.request.urlopen(req).read()
		
		elif method == "realtime":
			url = 'http://api.trackingmore.com/v2/trackings/realtime'
			RelUrl = url + urlStr
			result = requests.post(RelUrl,requestData.encode('utf-8'), headers=headers)
			# req = urllib.request.Request(RelUrl,requestData.encode('utf-8'), headers=headers,method="POST")
			# result = urllib.request.urlopen(req).read()
		# elif method == "post":
		# 	url = 'http://api.trackingmore.com/v2/trackings/post'
		# 	RelUrl = url + urlStr
		# 	req = urllib.request.Request(RelUrl,requestData.encode('utf-8'), headers=headers,method="POST")
		# 	result = urllib.request.urlopen(req).read()
		# elif method == "codeNumberPut":
		# 	url = 'http://api.trackingmore.com/v2/trackings'
		# 	RelUrl = url + urlStr
		# 	req = urllib.request.Request(RelUrl,requestData.encode('utf-8'), headers=headers,method="PUT")
		# 	result = urllib.request.urlopen(req).read()
		# elif method == "codeNumberDel":
		# 	url = 'http://api.trackingmore.com/v2/trackings'
		# 	RelUrl = url + urlStr
		# 	req = urllib.request.Request(RelUrl,requestData.encode('utf-8'), headers=headers,method="DELETE")
		# 	result = urllib.request.urlopen(req).read()
		
		return result



